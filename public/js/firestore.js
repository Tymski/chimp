// FIREBASE INITIALIZATION
var config = {
    apiKey: "AIzaSyCwhVGfH7ZNxzMJ7TpzDdd2QH9dxaswHi8",
    authDomain: "tymskichimp.firebaseapp.com",
    databaseURL: "https://tymskichimp.firebaseio.com",
    projectId: "tymskichimp",
    storageBucket: "tymskichimp.appspot.com",
    messagingSenderId: "839521571231"
};
firebase.initializeApp(config);
const db = firebase.firestore();
db.settings({
    timestampsInSnapshots: true
});