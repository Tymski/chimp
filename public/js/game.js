Width = localStorage.getItem("Width");
Height = localStorage.getItem("Height");
Numbers = localStorage.getItem("Numbers");
HideAfterTime = localStorage.getItem("HideAfterTime") == "true";
AutoPlay = localStorage.getItem("AutoPlay") == "true";
AutoPlayDelay = localStorage.getItem("AutoPlayDelay");
HideAfterSeconds = localStorage.getItem("HideAfterSeconds");

let highlightedClass = "highlighted";
let redClass = "red";
let emptyClass = "empty";
let dataValueAttribute = "data-value";

blockMouseClicks = false;
weClicked = false;
gameID = 0;
hideTimeout = null;
fillTimeout = null;

function generateGameTable() {
    html = '<div class="game" id="game">';
    style = "<style> .game{grid-template-columns: ";
    let x = 0;
    for (let i = 0; i < Width; i++) {
        style += " auto ";
        for (let j = 0; j < Height; j++) {
            html += '<button class="cell empty" id="c' + x++ + '"> 0 </button>'; // onClick="gameButtonClick(this)"
        }
    }
    html += "</div>";
    style += "} </style>";
    var nodes = htmlToElements(html)[0];

    body.appendChild(htmlToElements(style)[0]);
    body.appendChild(nodes);
    addEventListenersToGameButtons();
}
generateGameTable();

function addEventListenersToGameButtons() {
    let game = document.querySelector("#game");
    let buttons = game.querySelectorAll("button");
    Array.from(buttons).forEach(function(button) {
        button.addEventListener("pointerdown", function() {
            gameButtonClick(button);
        });
    });
}

function addRemoveRed(add) {
    ary.forEach(node => {
        if (add) {
            if (node.classList.contains(highlightedClass)) node.classList.add(redClass);
        } else {
            node.classList.remove(redClass);
        }
    });
}

/**
 * @param {String} HTML representing any number of sibling elements
 * @return {NodeList}
 */
function htmlToElements(html) {
    var template = document.createElement("template");
    template.innerHTML = html;
    return template.content.childNodes;
}

function gameButtonClick(node) {
    // block input while we wait between games
    if (blockMouseClicks) return;

    // We clicked on empty cell
    let value = node.getAttribute(dataValueAttribute);
    if (value === null) {
        return;
    }

    // We clicked on non empty cell
    weClicked = true;

    // Add Click to stats
    eventStack.push("" + value + " " + performance.now());

    // We clicked on a cell with correct number
    if (value == currentValue) {
        if (currentValue == 1) highlightCells();
        node.classList.remove(highlightedClass);
        if (currentValue == Numbers) {
            // We win
            blockMouseClicks = true;
            if (AutoPlay) {
                clearTimeout(fillTimeout);
                fillTimeout = setTimeout(() => {
                    fillRandomCells();
                }, AutoPlayDelay * 1000);
            }
            gameFinished();
        }
        currentValue++;
    } else {
        // We lose
        node.classList.add(redClass);
        addRemoveRed(true);

        blockMouseClicks = true;
        if (AutoPlay) {
            clearTimeout(fillTimeout);
            fillTimeout = setTimeout(() => {
                fillRandomCells();
            }, AutoPlayDelay * 1000);
        }
        gameFinished();
    }
    node.classList.remove(highlightedClass);
}

function gameFinished() {
    pushToFirestore();
}

function fillRandomCells() {
    //Start new game
    eventStack = [];
    eventStack.push("Start " + performance.now());
    blockMouseClicks = false;
    weClicked = false;
    currentValue = 1;
    ary = Array.from(game.childNodes);
    subary = getRandom(ary, Numbers);
    x = 0;
    ary.forEach(node => {
        node.innerHTML = "0";
        node.classList.add(emptyClass);
        node.classList.remove(redClass);
        addRemoveRed(false);
        node.classList.remove(highlightedClass);
        node.removeAttribute(dataValueAttribute);
    });
    subary.forEach(node => {
        node.innerHTML = ++x;
        node.classList.remove(emptyClass);
        node.setAttribute(dataValueAttribute, x);
    });
    if (HideAfterTime) {
        clearTimeout(hideTimeout);
        hideTimeout = setTimeout(function() {
            highlightCells();
        }, HideAfterSeconds * 1000);
    }
}
fillRandomCells();

function highlightCells() {
    clearTimeout(hideTimeout);
    game.childNodes.forEach(node => {
        if (node.getAttribute(dataValueAttribute) != null) {
            node.classList.add(highlightedClass);
            node.classList.add(emptyClass);
            // node.innerHTML = "0";
        }
    });
}

function clearCells() {
    ary.forEach(node => {
        // node.innerHTML = "0";
        node.classList.add(emptyClass);
        node.classList.remove(redClass);
        addRemoveRed(false);
        node.classList.remove(highlightedClass);
        node.removeAttribute(dataValueAttribute);
    });
}

function getRandom(arr, n) {
    var result = new Array(n),
        len = arr.length,
        taken = new Array(len);
    if (n > len) throw new RangeError("getRandom: more elements taken than available");
    while (n--) {
        var x = Math.floor(Math.random() * len);
        result[n] = arr[x in taken ? taken[x] : x];
        taken[x] = --len in taken ? taken[len] : len;
    }
    return result;
}

function toggleFullScreen() {
    if (!document.fullscreenElement) {
        document.documentElement.requestFullscreen = document.documentElement.requestFullscreen || document.documentElement.webkitRequestFullScreen || document.documentElement.mozRequestFullScreen || document.documentElement.msRequestFullscreen;
        document.documentElement.requestFullscreen();
    } else {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        }
    }
}

document.addEventListener(
    "keypress",
    function(e) {
        if (e.key === "f") {
            toggleFullScreen();
        }
        if (e.key === "s") {
            location.href = "settings.html";
        }
        if (e.key === " ") {
            e.stopPropagation();
            e.preventDefault();
            fillRandomCells();
        }
        if (e.key === "c") {
            clearCells();
        }
    },
    false
);

function pushToFirestore() {
    db.collection("stats").add({
        storage: JSON.stringify(localStorage),
        events: JSON.stringify(eventStack),
        date: Date.now()
    });
}
