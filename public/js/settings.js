function setDefaultLocalStorageValues(force = false) {
    if (localStorage.getItem("Width") == null || force)
        localStorage.setItem("Width", "9");
    if (localStorage.getItem("Height") == null || force)
        localStorage.setItem("Height", "7");
    if (localStorage.getItem("HideAfterTime") == null || force)
        localStorage.setItem("HideAfterTime", "false");
    if (localStorage.getItem("AutoPlay") == null || force)
        localStorage.setItem("AutoPlay", "true");
    if (localStorage.getItem("AutoPlayDelay") == null || force)
        localStorage.setItem("AutoPlayDelay", "1");
    if (localStorage.getItem("HideAfterSeconds") == null || force)
        localStorage.setItem("HideAfterSeconds", "5");
    if (localStorage.getItem("Numbers") == null || force)
        localStorage.setItem("Numbers", "6");
    if (localStorage.getItem("_Name") == null || force)
        localStorage.setItem("_Name", "Your name");
    if (localStorage.getItem("_Age") == null || force)
        localStorage.setItem("_Age", "Your age");
    if (localStorage.getItem("_Gender") == null || force)
        localStorage.setItem("_Gender", "Your gender");
    if (localStorage.getItem("_Message") == null || force)
        localStorage.setItem("_Message", "Anything you want to share");
}
setDefaultLocalStorageValues(false);

function generateLocalStorageEditor() {
    var nodes = document.createElement("div");
    nodes.classList.add("settings");
    for (var i = 0; i < localStorage.length; i++) {
        var key = localStorage.key(i)
        // var value = localStorage.getItem(key);
        var node = generateKeyValueEditForm(key);
        nodes.appendChild(node);
    }
    return nodes;
}

function generateKeyValueEditForm(key) {
    value = localStorage.getItem(key);
    div = document.createElement("div");
    div.classList.add("setting");
    div.innerHTML = `
        <label for="setting">Setting:</label> <input type = "text" value = "${key}"> 
        <label for="value">Value:</label> <input type = "text" value = "${value}">
        <button class="accept-btn" onClick="onClickSetValue(this)">Save</button>
    `;
    return div;
}

function onClickSetValue(arg) {
    var inputs = arg.parentNode.querySelectorAll("input");
    var key = inputs[0].value;
    var value = inputs[1].value;
    localStorage.setItem(key, value);
}

function initialize() {
    var settings = document.querySelector("#settings");
    if (settings != null) {
        var localStorageEditor = generateLocalStorageEditor();
        settings.appendChild(localStorageEditor)
    }
}
initialize();

function resetButton() {
    localStorage.clear();
    location.reload();
}

function saveAllSettings() {
    var settings = Array.from(document.querySelectorAll(".setting"));
    settings.forEach(element => {
        var key = element.querySelectorAll("input")[0].value;
        var value = element.querySelectorAll("input")[1].value;
        localStorage.setItem(key, value);
    });
}

function highlightUnderscoredSettings() {
    var settings = Array.from(document.querySelectorAll(".setting"));
    settings.forEach(element => {
        var node = element.querySelectorAll("input")[0];
        var firstLetter = node.value[0];
        if (firstLetter == "_") {
            node.parentNode.classList.add("underscored");
        }
    });
}
highlightUnderscoredSettings();