# Chimp test

Cognitive test  
  
Play/Test here:  
<https://tymski.gitlab.io/chimp>  

Change your settings there:  
<https://tymski.gitlab.io/chimp/settings.html>  
   
There are numbers displayed on the screen. Remember their location and click on the rectangles in correct order.  
**1, 2, 3, 4, 5...**
  
To see what this is all about watch Vsauce video on the subject.  
[![Vsauce video on Monkey tests and perception](http://img.youtube.com/vi/ktkjUjcZid0/0.jpg)](http://www.youtube.com/watch?v=ktkjUjcZid0 "Vsauce video on Monkey tests and perception")


## settings.html
  
<https://tymski.gitlab.io/chimp/settings.html>  
You may change a whole bunch of settings to customize the experience.  
  
You can change number of numbers, timing and other stuff.  

## TODO
- Better data gathering  
- Data analysis  
- Work on user experience
- Extract settings as seperate generic project with prefixes and default setings editor.
- Put the ToDo list in a more suitable place
- Make GDPR and Cookies stuff
- Make it work on different resolutions and ratios.
- Create a home page
